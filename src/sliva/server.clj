(ns sliva.server
  (:require [compojure.core :refer [defroutes GET]]
            [compojure.route :refer [resources]]
            [ring.util.response :refer [resource-response]]
            [ring.middleware.reload :refer [wrap-reload]]
            [sliva.server.handlers :refer [socket-handler]]))

;; wat
(defroutes routes
  ;;(GET "/" [] (resource-response "index.html" {:root "public"}))
  (GET "/hello" [] "wat")
  (GET "/ws" [] socket-handler)
  (resources "/"))


(def handler (-> #'routes
                 wrap-reload))
