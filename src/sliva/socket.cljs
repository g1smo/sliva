(ns sliva.socket
  (:require [chord.client :refer [ws-ch]]
            [clojure.string :as str]
            [cljs.core.async :refer [<! >! put! close!]]
            [sliva.data :refer [appstate]]
            [sliva.gfx :refer [spin-objects displace-objects vizual-reset vizual-update]])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(defn handle-message [msg-str]
  (let [[msg & args] (str/split msg-str #":")]
    ;;(console.info "msg" msg "args" args)
    (condp = msg
      "pong" (console.log "hura, server se odziva")
      "open" (swap! appstate assoc :clients (conj (:clients @appstate) (first args)))
      "spin" (spin-objects args)
      "displace" (displace-objects args)
      "vizual-reset" (vizual-reset)
      "vizual-update" (vizual-update args)
      (console.info "msg ingored: " msg))))

(defn send-message [& message]
  (let [vtic (:vtic @appstate)]
    (put! vtic (str/join ":" message))))

(defn websocket-init []
  (console.log "init webscoket")
  (go (let [host (aget js/window "location" "hostname")
            {:keys [ws-channel error]} (<! (ws-ch (str "ws://" host ":3449/ws")))]
        (if-not error
          (do
            (swap! appstate assoc :vtic ws-channel)
            (>! ws-channel "hello:server")

            (go-loop []
              (let [{:keys [message]} (<! ws-channel)]
                   (handle-message message)
                   (recur))))
          (js/console.log "Fejl websocket: " (pr-str error))))))

(defn click-test []
  (.addEventListener
   js/window
   "click"
   #(send-message "ping")))
