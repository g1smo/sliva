(ns sliva.data
  (:require [cljs.core.async :refer [chan]]
            [reagent.core :as reagent]))


(def initial-vizual-params
  ;;;;; ☭☭☭☭☭☭☭☭☭☭☭☭☭☭ ;;;;;;
  ;; ☭☭☭☭ Parametri razni ☭☭☭☭ ;;
  ;;;;; ☭☭☭☭☭☭☭☭☭☭☭☭☭☭ ;;;;;;
  {:odmik-kamere 100
   ;;:rotacija-kamere 1
   :rotacija-kamere 0.5
   :bg-barva 0x000000
   ;;:FOV 140
   :FOV 90
   :lik-sirina 0.5
   :obj-limit 10000
   :objekti []
   :stevec 0
   :rotacija-x 0.006
   :rotacija-y 0.001
   :rotacija-z 0.003
   :center-x 0
   :center-y 0
   :center-z 0
   ;;:zamik-barve 0.0000666
   :zamik-barve 0.000000006
   :zacetna-barva 0.333
   ;;:zacetna-barva 0.01
   :saturacija 1
   :svetlost 0.4
   :w-diff 0.5
   :gostota-obj 2})

;; App state atom
(def appstate
  (reagent/atom
   {:clients []
    :vtic (chan)
    :vizual (merge {:animiraj false} initial-vizual-params)}))
