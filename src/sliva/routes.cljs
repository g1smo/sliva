(ns sliva.routes
  (:import goog.history.Html5History)
  (:require [secretary.core :as secretary :refer-macros [defroute]]
            [goog.events :as events]
            [goog.history.EventType :as EventType]
            [sliva.data :refer [appstate]]))

(defn hook-browser-navigation! []
  (doto (Html5History.)
    (events/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn app-routes []
  ;;(secretary/set-config! :prefix "#☭")
  (secretary/set-config! :prefix "#")
  (defroute "/" [] (swap! appstate assoc :page :hub))
  (defroute "/hub" [] (swap! appstate assoc :page :hub))
  (defroute "/visual" [] (swap! appstate assoc :page :visual))
  (defroute "/test" [] (console.log "test!"))
  (defroute "/gibanica" [] (swap! appstate assoc :page :gibanica))
  (hook-browser-navigation!))
