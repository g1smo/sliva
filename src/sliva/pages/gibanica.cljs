(ns sliva.pages.gibanica
  (:require [reagent.core :as reagent]
            [sliva.data :refer [appstate]]
            [sliva.pages.navigation :refer [navigation]]
            [sliva.socket :refer [send-message]]
            [sliva.gfx :refer [get-param]]))

(defn motion-track [event]
  (let [pospesek (.-acceleration event)
        gibX (.-x pospesek)
        gibY (.-y pospesek)
        gibZ (.-z pospesek)
        cas (-> (js/Date.)
                (.valueOf))]
    (send-message "motion" cas gibX gibY gibZ)))

(defn start-motion-track []
  (.addEventListener js/window "devicemotion" motion-track))

(defn stop-motion-track []
  (.removeEventListener js/window "devicemotion" motion-track))

(def rotation (reagent/atom nil))
(defn rotation-track [event]
  (let [alpha (.-alpha event)
        beta (.-beta event)
        gamma (.-gamma event)
        cas (-> (js/Date.)
                (.valueOf))]
    (console.log "rotation:" @rotation)
    (if @rotation
      (let [da (- (:a @rotation) alpha)
            db (- (:b @rotation) beta)
            dg (- (:g @rotation) gamma)]
        (send-message
         "rotation"
         cas
         da
         db
         dg))
      (swap! rotation merge {:a alpha
                             :b beta
                             :g gamma}))))

(defn start-rotation-track []
  (.addEventListener js/window "deviceorientation" rotation-track))

(defn stop-rotation-track []
  (.removeEventListener js/window "deviceorientation" rotation-track))

(defn gibanica []
  (reagent/create-class
   {:display-name "gibanica"
    :component-did-mount (fn []
                           (start-motion-track)
                           (start-rotation-track))
    :component-will-unmount (fn []
                              (stop-motion-track)
                              (stop-rotation-track))
    :should-component-update #(true)
    :reagent-render
    (fn []
      [:div
       [:h3 "gibaj me!"]
       ;; [:div
       ;;  [:h4 "Rotacija"]
       ;;  [:div "Alfa:" (:a @rotation)]
       ;;  [:div "Beta:" (:b @rotation)]
       ;;  [:div "Gama:" (:g @rotation)]]
       [:div
        "Barva"
        [:input {:type "range"
                 :min 0.01
                 :max 0.333
                 :step 0.001
                 :value (get-param :zacetna-barva)
                 :on-change (fn [event]
                             (send-message "vizual-update" "zacetna-barva" (aget event "target" "value")))}]]
       [:button {:on-click #(send-message "vizual-reset")} "Resetiraj"]
       [navigation]])}))

