(ns sliva.pages.navigation)

(defn navigation []
  [:div
   [:h3 "Navigation"]
   [:ul
    [:li [:a {:href "#/hub"} "Hub"]]
    [:li [:a {:href "#/visual"} "Visual"]]
    [:li [:a {:href "#/gibanica"} "Gibanica"]]]])
