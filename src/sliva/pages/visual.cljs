(ns sliva.pages.visual
  (:require [reagent.core :as reagent]
            [sliva.gfx :refer [start-render stop-render]]
            [sliva.pages.navigation :refer [navigation]]))

(defn visual []
  (reagent/create-class
   {:display-name "vizual-render-plac"
    :reagent-render (fn [] [:div
                           [:div {:ref "vizual"}]
                           [navigation]])
    :component-did-mount (fn [this] (start-render (.. this -refs -vizual)))
    :component-will-unmount (fn [this]
                              (console.log "unmounting?")
                              (stop-render (.. this -refs -vizual)))}))
