(ns sliva.pages.hub
  (:require [sliva.data :refer [appstate]]
            [sliva.pages.navigation :refer [navigation]]))

(defn hub []
  [:div
   [:h1 "Connected clients"]
   [:table
    [:thead
     [:tr
      [:th "Client ID"]
      [:th "status"]]]
    [:tbody
     (let [clients (:clients @appstate)]
       (if (empty? clients)
         [:tr [:td "No"] [:td "clients"]]
         (->> clients
             (map-indexed (fn [idx cid]
                            [:tr {:key idx}
                             [:td cid]
                             [:td "connected"]])))))]]
   [navigation]])
