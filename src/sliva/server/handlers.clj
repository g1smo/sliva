(ns sliva.server.handlers
  (:require [clojure.java.shell :refer [sh]]
            [compojure.core :refer [defroutes GET]]
            [compojure.route :refer [resources]]
            [clojure.string :as str]
            [clojure.core.async :refer [chan <! <!! >! put! close! go go-loop sliding-buffer]]
            [ring.util.response :refer [resource-response]]
            [ring.middleware.reload :refer [wrap-reload]]
            [chord.http-kit :refer [with-channel]]))

(def clients (atom {}))

(defn send-all [cmd args]
  (doseq [conn @clients]
    ;;(println "sending" cmd (str/join ":" args))
    (put! (val conn)
          (str cmd ":" (str/join ":" args)))))

(defn socket-handler [request]
  (let [cid (java.util.UUID/randomUUID)]
    (with-channel request ws-ch
      {:read-ch (chan (sliding-buffer 100))
       :write-ch (chan (sliding-buffer 100))}
      (go
        (let [{:keys [message]} (<! ws-ch)]
          (println "new connection - " cid)
          (>! ws-ch (str "hello:" cid))
          (swap! clients assoc cid ws-ch)
          (send-all "open" [cid])
          (go-loop []
            (let [{:keys [message]} (<! ws-ch)]
              (let [[cmd & args] (str/split message #":")]
                ;;(println "command" cmd args)
                (condp = cmd
                  "ping" (>! ws-ch "pong")
                  "motion" (send-all "displace" args)
                  "rotation" (send-all "spin" args)
                  "vizual-reset" (send-all "vizual-reset" [])
                  "vizual-update" (send-all "vizual-update" args)
                  (println "DEBUG: msg ignored: " cmd)))
              (recur))))))))
;;(close! ws-ch)))))

;;test
;;(while true
;;  (let [return (sh "sudo" "bash" "-c" "iwlist scan | grep ESSID | sed 's/[[:space:]]\\+ESSID://' | sed 's/\"//g' | uniq")]
;;    (println "GOT NETWORKS!" (:out return)))
