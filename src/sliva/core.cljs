(ns sliva.core
  (:require [secretary.core :as secretary :refer-macros [defroute]]
            [reagent.core :as reagent]
            [sliva.socket :refer [websocket-init click-test]]
            [sliva.data :refer [appstate]]
            [sliva.routes :refer [app-routes]]
            [sliva.pages.hub :refer [hub]]
            [sliva.pages.visual :refer [visual]]
            [sliva.pages.gibanica :refer [gibanica]])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(enable-console-print!)

(console.log "Hello, Sky!")

;; Page switching
(defmulti current-page #(@appstate :page))
(defmethod current-page :hub [] [hub])
(defmethod current-page :visual [] [visual])
(defmethod current-page :gibanica [] [gibanica])

;; On figwheel reload do this (reload routes, render currently active page)
(defn init-app []
  (app-routes)
  (console.log "App (re)init!")
  (reagent/render [current-page] (.getElementById js/document "container")))

;; Vstopna tocka
(aset js/document "onreadystatechange"
      (fn []
        (if (= (.-readyState js/document) "complete")
          (do
            (websocket-init)
            (click-test)
            (.enable (js/NoSleep.))
            (init-app)))))
